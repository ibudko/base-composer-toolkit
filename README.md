# Introduction
This is a composer metapackage toolkit to quick start 
backend development process
## Tools
`"vlucas/phpdotenv": "2.*"`

Is a library to supply environmental part of your project and 
to help deploying the project

`"klein/klein": "2.*"`

It's a very convenient library to build the routing system

`"doctrine/cache": "1.*"`

The lib is for caching any information and making your app
faster

`"propel/propel1": "1.*"`

ORM library to access database

`"phing/phing": "2.*"`

PHing Is Not GNU make; it's a PHP project build system or build tool based on Apache Ant.

`"nafisc/parameterparser": "0.*"`

A library for parsing strings of parameters

`"league/event": "2.*"`

The league/event package provides a versatile tool 
to manage events in your app or domain. The package 
supports string based and class based events, as well as 
Closure or class based listeners. This makes the package 
ideal for regular event management but also allows for 
a clean event-driven style of programming.